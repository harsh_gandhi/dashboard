import React from 'react';
import { PageHeader, Tabs } from 'antd';
import { history, useLocation } from 'umi';
import './index.less';

const { TabPane } = Tabs;

const AccountPageNavbar: React.FC = () => {
  const location = useLocation();

  const activeTabKey = location.pathname.split('/')[2];
  const handleChange = (key: string) => {
    history.push(`/account/${key}`);
  };

  return (
    <>
      <PageHeader
        className="site-page-header-responsive background-white headerArea"
        title="Account Settings"
        footer={
          <Tabs activeKey={activeTabKey} className="background-white" onChange={handleChange}>
            <TabPane tab="Overview" key="overview" />
            <TabPane tab="Billing History" key="billing" />
            <TabPane tab="Referral Program" key="referral" />
          </Tabs>
        }
      />
    </>
  );
};
export default AccountPageNavbar;
