import React from 'react';
import { Dropdown, Row, Col, Divider, Button } from 'antd';
import { DownOutlined, SettingOutlined } from '@ant-design/icons';
import { Link } from 'umi';
import './index.less';

const menu = (
  <div className="dropdown-box">
    <Row className="upper-row">
      <Col span={8}>
        <div className="photo">
          <img src="" alt="" />
        </div>
      </Col>
      <Col span={16}>
        <div className="user-details">
          <div className="user-name">
            <b>User name</b>
          </div>
          <div className="user-id">
            <b>User id</b>
          </div>
          <div className="edit-profile-link" style={{ marginTop: '10px' }}>
            <Link to="#edit-profile">Edit profile</Link>
          </div>
        </div>
      </Col>
    </Row>
    <Divider style={{ margin: '0' }} />
    <Row className="lower-row">
      <Col span={24}>
        <div className="account-settings-links">
          <Link to="/account/overview">
            <Button type="primary" shape="round">
              <SettingOutlined />
              Account Settings
            </Button>
          </Link>
          <Link to="">
            <Button>Sign Out</Button>
          </Link>
        </div>
      </Col>
    </Row>
  </div>
);

const ProfileMenu: React.FC = () => {
  return (
    <Dropdown overlay={menu} trigger={['click']}>
      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
        User name <DownOutlined />
      </a>
    </Dropdown>
  );
};

export default ProfileMenu;
