import React from 'react';
import './settingsComponent.less';
import { Switch } from 'antd';
import { Link } from 'umi';

interface Props {
  title: string;
  description: string;
}

const SettingsComponent: React.FC<Props> = ({ title, description }) => {
  return (
    <div className="setting-box-style">
      <Switch defaultChecked className="right" />
      <div className="title-style">
        <b>{title}</b>
      </div>
      <div>
        <span>
          Send this notification to <Link to="/user">userid</Link> {description}
        </span>
      </div>
    </div>
  );
};

export default SettingsComponent;
