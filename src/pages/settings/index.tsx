import React from 'react';
import { Input, Divider, Checkbox, Row, Col, Button } from 'antd';
import './index.less';

const { TextArea } = Input;

const SettingsPage: React.FC = () => {
  return (
    <>
      <div className="backgroundColor">
        <Row>
          <Col span={6}>
            <h1 className="text">
              <b>Basic Information</b>
            </h1>
          </Col>
          <Col span={18}>
            <Input size="large" placeholder="PROJECT NAME" />
            <br />
            <br />
            <Input size="large" placeholder="WEBSITE URL" />
            <br />
            <br />
            <TextArea rows={6} placeholder="DESCRIPTION" />
          </Col>
        </Row>

        <Divider />

        <Row>
          <Col span={6}>
            <h1 className="text">
              <b>Feedback Category</b>
            </h1>
          </Col>
          <Col span={6}>
            <Checkbox>Bug</Checkbox>
            <br />
            <Checkbox>Improvement</Checkbox>
            <br />
            <Checkbox>Task</Checkbox>
          </Col>
          <br />
          <a href="">Add a new Category</a>
          <Col span={6} />
          <Col span={6} />
        </Row>

        <Divider />

        <Row>
          <Col span={6}>
            <h1 className="text">
              <b>User Access</b>
            </h1>
          </Col>
          <Col span={6}>
            <Checkbox>Username(Admin)</Checkbox>
          </Col>
          <Col span={6} />
          <Col span={6} />
        </Row>

        <Divider />

        <Row>
          <Col span={6}>
            <Button type="primary" shape="round" size="large">
              Delete
            </Button>
            <Button type="primary" shape="round" size="large">
              Archive
            </Button>
          </Col>
          <Col span={6} />
          <Col span={6} />
          <Col span={6}>
            <Button type="primary" shape="round" size="large">
              Save Changes
            </Button>
          </Col>
        </Row>
      </div>
    </>
  );
};
export default SettingsPage;
