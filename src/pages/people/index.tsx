import React from 'react';
import { PageHeader, Button, Divider, Input, Row, Col, Space } from 'antd';
import './index.less';

const { Search } = Input;

const content = <p>Last Login:11 hours ago</p>;

const Content: React.FC = ({ children }) => {
  return (
    <Row>
      <div style={{ flex: 1 }}>{children}</div>
    </Row>
  );
};

const People = () => {
  return (
    <>
      <div className="site-page-header-ghost-wrapper">
        <PageHeader
          ghost={false}
          title="People"
          extra={[
            <Button type="primary" shape="round" size="large">
              Invite People
            </Button>,
          ]}
        />
      </div>
      <Divider />
      <Search placeholder="Search People" size="large" onSearch={(value) => console.log(value)} />
      <br />
      <br />
      <Row>
        <Col span={6}>
          <Space direction="vertical">
            <p>
              View All (1)
              <br />
              Admin (1)
              <br />
              Collaborator (0)
              <br />
              Client (0)
              <br />
              ---
              <br />
              Deactivated (0)
              <br />
              Pending Invitation (0)
            </p>
          </Space>
        </Col>
        <Col span={12}>
          <br />
          <h3 className="heading">Active(1)</h3>
          <Row>
            <Col span={24}>
              <div className="site-page-header-ghost-wrapper">
                <PageHeader
                  className="user_info"
                  ghost={false}
                  title="Admin"
                  extra={[
                    <Button type="primary" shape="round">
                      Owner
                    </Button>,
                    <Button type="primary" shape="round">
                      Admin
                    </Button>,
                  ]}
                >
                  <Content>{content}</Content>
                </PageHeader>
              </div>
            </Col>
          </Row>
          <br />
          <h3 className="heading">Deactivated(0)</h3>
          <br />
          <h3 className="heading">Pending Invitation(0)</h3>
        </Col>
        <Col span={6} />
      </Row>
    </>
  );
};

export default People;
