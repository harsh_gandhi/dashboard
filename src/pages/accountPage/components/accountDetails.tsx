import React from 'react';
import './accountDetails.less';
import { Form, Input, Select, Button, Typography } from 'antd';

const { Option } = Select;
const { Title } = Typography;

const AccountDetails: React.FC = () => {
  return (
    <div className="account-details-content">
      <div className="form-header">
        <Title level={4}>Account Details</Title>
      </div>
      <Form name="basic" layout="vertical">
        <Form.Item label="COMPANY NAME" name="company-name">
          <Input />
        </Form.Item>
        <Form.Item label="COMPANY WEBSITE" name="company-website">
          <Input />
        </Form.Item>
        <Form.Item label="CONTACT NUMBER" name="contact-number">
          <Input type="number" />
        </Form.Item>
        <Form.Item label="COUNTRY" name="country">
          <Select placeholder="Select..." allowClear>
            <Option value="india">India</Option>
          </Select>
        </Form.Item>
        <Form.Item label="TIMEZONE" name="timezone">
          <Select placeholder="Select..." allowClear>
            <Option value="timezone1">timezone1</Option>
          </Select>
        </Form.Item>
        <Form.Item name="company-address" label="COMPANY ADDRESS (BILLING ADDRESS)">
          <Input.TextArea />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" style={{ borderRadius: '20px' }}>
            Save Changes
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default AccountDetails;
