import React from 'react';
import './accountSummary.less';
import { Typography, Row, Col, Divider, Button, Progress } from 'antd';

const { Title } = Typography;

const AccountSummary: React.FC = () => {
  return (
    <div className="account-summary-content">
      <div className="summary-header">
        <Title level={4}>Account Summary</Title>
      </div>
      <div className="account-info">
        <Row gutter={10} style={{ marginBottom: '5px' }}>
          <Col span={12} className="gutter-row">
            Account Id
          </Col>
          <Col span={12} className="gutter-row">
            123456
          </Col>
        </Row>
        <Row gutter={10} style={{ marginBottom: '5px' }}>
          <Col span={12} className="gutter-row">
            My Account Plan
          </Col>
          <Col span={12} className="gutter-row">
            Corporate Plan
          </Col>
        </Row>
        <Row gutter={10} style={{ marginBottom: '5px' }}>
          <Col span={12} className="gutter-row">
            Add-ons
          </Col>
          <Col span={12} className="gutter-row">
            _
          </Col>
        </Row>
        <Row gutter={10} style={{ marginBottom: '5px' }}>
          <Col span={12} className="gutter-row">
            Payment Method
          </Col>
          <Col span={12} className="gutter-row">
            _
          </Col>
        </Row>
      </div>
      <div className="progress-bars">
        <div>Unlimited Users</div>
        <Progress percent={2} status="active" size="small" strokeColor="orange" />
        <div>Unlimited Projects</div>
        <Progress percent={2} status="active" size="small" strokeColor="orange" />
      </div>
      <Divider />
      <div className="summary-footer">
        <Button className="close-account" style={{ borderRadius: '20px' }}>
          Close Account
        </Button>
        <Button type="primary" style={{ borderRadius: '20px' }} className="change-plan">
          Manage Subscription
        </Button>
      </div>
    </div>
  );
};

export default AccountSummary;
