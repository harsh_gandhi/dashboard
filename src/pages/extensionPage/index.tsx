import React from 'react';
import { PageHeader, Divider, Row, Col, Input, Card, Button } from 'antd';
import './index.less';
import { PlusSquareFilled } from '@ant-design/icons';
import chrome from './icons8-chrome.svg';
import firefox from './icons8-firefox.svg';
import chromiumedge from './icons8-microsoft-edge.svg';

const Extensions = () => {
  return (
    <>
      <div>
        <PageHeader ghost={false} title="Extension Settings" />
      </div>
      <Divider />
      <h3 className="heading">Step 1:</h3>
      <div>
        <PageHeader className="heightarea" ghost={false} title="">
          <p>Install browser extension.</p>
          <div className="site-card-wrapper">
            <Row>
              <Col span={6}>
                <Card style={{ width: 200 }}>
                  <img src={chrome} alt="chrome" />
                  <p>Chrome</p>
                  <Button type="primary" size="small" className="button" block>
                    Install
                  </Button>
                </Card>
              </Col>
              <Col span={6}>
                <Card style={{ width: 200 }}>
                  <img src={firefox} alt="firefox" />
                  <p>Firefox</p>
                  <Button type="primary" size="small" className="button" block>
                    Install
                  </Button>
                </Card>
              </Col>
              <Col span={6}>
                <Card style={{ width: 200 }}>
                  <img src={chromiumedge} alt="chromium edge" />
                  <p>Chromium Edge</p>
                  <Button type="primary" size="small" className="button" block>
                    Install
                  </Button>
                </Card>
              </Col>
              <Col span={6} />
            </Row>
          </div>
        </PageHeader>
      </div>
      <Divider />
      <h3 className="heading">Step 2:</h3>
      <div>
        <PageHeader className="height" ghost={false} title="">
          <p>Copy and paste your account key below to the extension option.</p>
          <Input className="input" />
        </PageHeader>
      </div>
      <Divider />
      <h3 className="heading">Step 3:</h3>
      <div>
        <PageHeader className="height" ghost={false} title="">
          <p>
            Add domains to your projects. Feedback will be collected under the project that has a
            matching domain, or the default project when no matching domain is found.
          </p>
          <Row>
            <Col span={4}>
              <h3 className="heading">Sample Project</h3>
              <PlusSquareFilled />
            </Col>
            <Col span={4} />
            <Col span={4} />
            <Col span={4} />
            <Col span={4} />
            <Col span={4}>
              <h3> Default Project</h3>
            </Col>
          </Row>
        </PageHeader>
      </div>
    </>
  );
};

export default Extensions;
