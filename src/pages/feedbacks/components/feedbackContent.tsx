import React from 'react';
import NoFeedback from './NoFeedback';

const FeedbackContent: React.FC = () => {
  return (
    <div>
      <NoFeedback />
    </div>
  );
};
export default FeedbackContent;
