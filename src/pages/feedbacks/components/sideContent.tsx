import React from 'react';
import { Anchor } from 'antd';
import './sideContent.less';

const { Link } = Anchor;

const SideContent: React.FC = () => {
  const getCurrentAnchor = () => {
    return '#open';
  };

  const handleChange = () => {
    // console.log('anchor:onchange', link);
  };

  return (
    <>
      <Anchor affix={false} getCurrentAnchor={getCurrentAnchor} onChange={handleChange}>
        <Link href="#open" title="Open(0)" />
        <Link href="#progress" title="In Progress(0)" />
        <Link href="#review" title="Under Review(0)" />
        <Link href="#resolved" title="Resolved(0)" />
      </Anchor>
      <div className="edit-workflow">
        <Anchor affix={false}>
          <Link href="#workflow-edit" title="Edit Workflow" />
        </Anchor>
      </div>
      <div className="export-feedback">
        <Anchor affix={false}>
          <Link href="#export-feedback" title="Export Feedback" />
        </Anchor>
      </div>
    </>
  );
};

export default SideContent;
