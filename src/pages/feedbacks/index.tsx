import React from 'react';
import ActionBar from './components/mainActionBar';
import MainContent from './components/mainContent';
import './index.less';

const Feedbacks: React.FC = () => {
  return (
    <div className="feedback-page-container">
      <ActionBar />
      <MainContent />
    </div>
  );
};

export default Feedbacks;
