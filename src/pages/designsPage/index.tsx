import React from 'react';
import { PageHeader, Button, Divider, Input, Row, Col } from 'antd';
import './index.less';
import designs from './designs.png';

const { Search } = Input;

const Design = () => {
  return (
    <>
      <div className="site-page-header-ghost-wrapper">
        <PageHeader
          ghost={false}
          title="Designs"
          extra={[
            <Button type="primary" shape="round" size="large">
              Upload Images
            </Button>,
          ]}
        />
      </div>
      <Divider />
      <Search placeholder="Search People" size="large" onSearch={(value) => console.log(value)} />
      <br />
      <br />
      <h3 className="font">Designs(0)</h3>
      <Divider dashed />
      <Row>
        <Col span={6} />
        <Col span={12}>
          <h3 className="font">Get full-page feedback on designs and images</h3>
          <br />
          <p>To get started, simply upload an image.</p>
          <img src={designs} alt="designs" className="image" />
        </Col>
        <Col span={6} />
      </Row>
    </>
  );
};

export default Design;
