import React from 'react';
import './index.less';
import { Row, Col } from 'antd';
import FeedbackComponent from './components/feedbackComponent';
import RatingComponent from './components/ratingComponent';
import SideInfoComponent from './components/sideInfoComponent';
import PeopleDetail from './components/peopleComponent';

const Dashboard: React.FC<{}> = () => {
  return (
    <>
      <Row className="all-content">
        <Col xs={24} sm={24} md={18} lg={18} xl={18} className="main-content">
          <FeedbackComponent
            totalCount={2}
            openCount={2}
            inProgressCount={0}
            underReviewCount={0}
            resolvedCount={0}
          />
          <RatingComponent />
          <PeopleDetail />
        </Col>
        <Col xs={24} sm={24} md={6} lg={6} xl={6} className="side-content">
          <SideInfoComponent />
        </Col>
      </Row>
    </>
  );
};

export default Dashboard;
