import React from 'react';
import { Column } from '@ant-design/charts';

const Graph: React.FC = () => {
  const data = [
    { year: '1991', value: 3 },
    { year: '1992', value: 4 },
    { year: '1993', value: 3.5 },
  ];
  const config = {
    data,
    title: {
      visible: true,
      text: 'Line chart with data points',
    },
    xField: 'year',
    yField: 'value',
  };
  return <Column {...config} />;
};

export default Graph;
