import React from 'react';
import { Typography, Row, Col, Rate, Divider } from 'antd';
import './ratingComponent.less';

const { Title } = Typography;

const RatingComponent: React.FC = () => {
  return (
    <div className="rating-container">
      <Title level={4}>Rating</Title>
      <div className="box-style">
        <Row>
          <Col xs={24} sm={24} md={6} lg={4} xl={4}>
            <div className="leftBox">
              <h1>Average</h1>
              <Rate disabled defaultValue={2} />
            </div>
            <Divider type="vertical" />
          </Col>
          <Col xs={24} sm={24} md={18} lg={20} xl={20}>
            <div className="rightBox">Graph</div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default RatingComponent;
