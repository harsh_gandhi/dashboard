import React from 'react';
import { PageHeader, Button, Typography } from 'antd';
import './index.less';
import ProjectDetail from './components/projectDetailComponent';

const { Title } = Typography;

const ProjectComponent: React.FC = () => {
  return (
    <>
      <PageHeader
        className="site-page-header-responsive side-page-header-ghost-wrapper project-page-headerArea"
        ghost={false}
        title={<Title level={3}>Projects</Title>}
        extra={[
          <Button key="1" type="primary" style={{ borderRadius: '20px' }}>
            Create Project
          </Button>,
        ]}
      />
      <ProjectDetail />
    </>
  );
};

export default ProjectComponent;
