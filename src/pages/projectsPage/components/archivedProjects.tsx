import React from 'react';

const ArchivedProjects: React.FC = () => {
  return (
    <div style={{ marginBottom: '20px' }}>
      <h3>Archived(0)</h3>
      <div>Archived Projects</div>
    </div>
  );
};

export default ArchivedProjects;
