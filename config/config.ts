import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const { REACT_APP_ENV } = process.env;

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  layout: {
    name: 'Ant Design Pro',
    locale: true,
    siderWidth: 208,
    pageTitleRender: false,
  },
  locale: {
    // default zh-CN
    default: 'zh-CN',
    // default true, when it is true, will use `navigator.language` overwrite default
    antd: true,
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/user',
      layout: false,
      routes: [
        {
          name: 'login',
          path: '/user/login',
          component: './user/login',
        },
      ],
    },
    {
      path: '/admin',
      name: 'admin',
      icon: 'crown',
      access: 'canAdmin',
      component: './Admin',
      routes: [
        {
          path: '/admin/sub-page',
          name: 'sub-page',
          icon: 'smile',
          component: './Welcome',
        },
      ],
    },
    {
      path: '/projects',
      name: 'Projects',
      component: './projectsPage/index.tsx',
    },
    {
      path: '/design',
      name: 'Designs',
      component: './designsPage/index.tsx',
    },
    {
      path: '/people',
      name: 'People',
      component: './people/index.tsx',
    },
    {
      path: '/extension',
      name: 'Extension',
      component: './extensionPage/index.tsx',
    },
    {
      path: '/account',
      component: '../layouts/accountPageLayout',
      routes: [
        {
          path: '/account',
          redirect: '/account/overview',
        },
        {
          path: '/account/overview',
          name: 'AccountOverview',
          component: './accountPage',
        },
      ],
    },
    {
      path: '/',
      component: '../layouts/dashboardLayout',
      routes: [
        {
          path: '/',
          redirect: '/dashboard',
        },
        {
          path: '/welcome',
          name: 'welcome',
          icon: 'smile',
          component: './Welcome',
        },
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: './dashboard/index.tsx',
        },
        {
          path: '/feedback',
          name: 'Feedback',
          component: './feedbacks/index.tsx',
        },
        {
          path: '/notifications',
          name: 'Notifications',
          component: './notification-settings/index.tsx',
        },
        {
          path: '/integrations',
          name: 'Integrations',
          component: './integrations/index.tsx',
        },
        {
          path: '/settings',
          name: 'Settings',
          component: './settings/index.tsx',
        },
        {
          component: './404',
        },
      ],
    },
    {
      name: 'list.table-list',
      icon: 'table',
      path: '/list',
      component: './ListTableList',
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
});
